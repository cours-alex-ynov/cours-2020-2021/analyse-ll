table_analyseLL = {
    "chien": {
        "_nom_commun": ["chien"]
    },
    "chat": {
        "_nom_commun": ["chat"]
    },
    "Pierre": {
        "_nom_propre": ["Pierre"],
        "_groupe_nominal": ["_nom_propre"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "Avignon": {
        "_nom_propre": ["Avignon"],
        "_groupe_nominal": ["_nom_propre"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "le": {
        "_article_defini": ["le"],
        "_article": ["_article_defini"],
        "_groupe_nominal": ["_article", "_nom_commun", "_adjectif"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "un": {
        "_article_defini": ["un"],
        "_article": ["_article_defini"],
        "_groupe_nominal": ["_article", "_nom_commun", "_adjectif"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "noir": {
        "_adjectif": ["noir"],
        "_groupe_nominal": ["_adjectif"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "blanc": {
        "_adjectif": ["blanc"],
        "_groupe_nominal": ["_adjectif"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "connait": {
        "_verbe": ["connait"],
        "_groupe_verbal": ["_verbe", "_adverbe"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
}

en_table_analyseLL = {
    "dog": {
        "_nom_commun": ["dog"]
    },
    "cat": {
        "_nom_commun": ["cat"]
    },
    "Rock": {
        "_nom_propre": ["Rock"],
        "_groupe_nominal": ["_nom_propre"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "Avignon": {
        "_nom_propre": ["Avignon"],
        "_groupe_nominal": ["_nom_propre"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "the": {
        "_article_defini": ["the"],
        "_article": ["_article_defini"],
        "_groupe_nominal": ["_article", "_adjectif", "_nom_commun"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "an": {
        "_article_defini": ["an"],
        "_article": ["_article_defini"],
        "_groupe_nominal": ["_article", "_adjectif", "_nom_commun"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "black": {
        "_adjectif": ["black"],
        "_groupe_nominal": ["_adjectif"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "white": {
        "_adjectif": ["white"],
        "_groupe_nominal": ["_adjectif"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "knows": {
        "_verbe": ["knows"],
        "_groupe_verbal": ["_verbe", "_adverbe"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
}


class TALN:
    def __init__(self, sentence, table_analyseLL, logger_enabled = True):
        self.sentence = sentence
        self.table = table_analyseLL
        self.analyseTable = ["_phrase"]
        self.logger_enabled = logger_enabled

    def analyse(self):
        self.log(self.sentence, self.analyseTable)
        self.readSentence(self.sentence[0])
        self.log(self.sentence, self.analyseTable)
        tmp_sentence = self.sentence
        for word in tmp_sentence:
            if len(self.analyseTable) == 0 and len(self.sentence) != 0:
                raise Exception('Sommet manquant !')
            elif len(self.analyseTable) != 0 and len(self.sentence) == 0:
                raise Exception('Texte manquant !')
            else:
                self.readWord(word)


    def readWord(self, word):
        if (self.analyseTable[0] == self.sentence[0]):
            self.log(self.sentence, self.analyseTable)
            self.analyseTable = self.analyseTable[1:]
            self.sentence = self.sentence[1:]
            return
        else:
            self.readNominalGroup(word)

        self.readWord(word)

    def readNominalGroup(self, word):
        self.log(self.sentence, self.analyseTable)
        if (self.table[word].get(self.analyseTable[0])):
            self.log('bonne pioche')
            tmp = self.analyseTable
            self.analyseTable = self.table[word][self.analyseTable[0]].copy()
            self.analyseTable += tmp[1:]
            return
        elif (self.analyseTable[0] == '_adverbe' or self.analyseTable[0] == '_adjectif'):
            self.log('EPSILON')
            self.analyseTable = self.analyseTable[1:]
            return
        else:
            raise Exception('Manque des trucs (mot : "' + word + '") pour le groupe ' + self.analyseTable[0] + " lo !")


    def readSentence(self, word):
        self.analyseTable = self.table[word][self.analyseTable[0]].copy()

    def log(self, *values):
        if (self.logger_enabled):
            print("[DEBUG]", values, "\n")


test = TALN(["le", "chat", "noir", "connait", "le", "chat", "noir"], table_analyseLL)
test.analyse()
