
map_fr_en = {
    "chien": ["dog", "_nom_commun"],
    "chat": ["cat", "_nom_commun"],
    "Pierre": ["Pierre", "_nom_propre"],
    "Avignon": ["Avignon", "_nom_propre"],
    "le": ["the", "_article_defini"],
    "un": ["a", "_article_indefini"],
    "noir": ["black", "_adjectif"],
    "blanc": ["white", "_adjectif"],
    "connait": ["knows", "_verbe"],
    "bien": ["well", "_adverbe"]
}


table_analyseLL = {
    "chien": {
        "_nom_commun": ["chien"]
    },
    "chat": {
        "_nom_commun": ["chat"]
    },
    "Pierre": {
        "_nom_propre": ["Pierre"],
        "_groupe_nominal": ["_nom_propre"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "Avignon": {
        "_nom_propre": ["Avignon"],
        "_groupe_nominal": ["_nom_propre"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "le": {
        "_article_defini": ["le"],
        "_article": ["_article_defini"],
        "_groupe_nominal": ["_article", "_nom_commun", "_adjectif"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "un": {
        "_article_defini": ["un"],
        "_article": ["_article_defini"],
        "_groupe_nominal": ["_article", "_nom_commun", "_adjectif"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "noir": {
        "_adjectif": ["noir"],
        "_groupe_nominal": ["_adjectif"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "blanc": {
        "_adjectif": ["blanc"],
        "_groupe_nominal": ["_adjectif"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "connait": {
        "_verbe": ["connait"],
        "_groupe_verbal": ["_verbe", "_adverbe"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "bien": {
        "_adverbe": ["bien"],
        "_groupe_verbal": ["_verbe", "_adverbe"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    }
}

en_table_analyseLL = {
    "chien": {
        "_nom_commun": ["dog"]
    },
    "chat": {
        "_nom_commun": ["cat"]
    },
    "Pierre": {
        "_nom_propre": ["Pierre"],
        "_groupe_nominal": ["_nom_propre"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "Avignon": {
        "_nom_propre": ["Avignon"],
        "_groupe_nominal": ["_nom_propre"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "le": {
        "_article_defini": ["the"],
        "_article": ["_article_defini"],
        "_groupe_nominal": ["_article", "_adjectif", "_nom_commun"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "un": {
        "_article_defini": ["a"],
        "_article": ["_article_defini"],
        "_groupe_nominal": ["_article", "_adjectif", "_nom_commun"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "noir": {
        "_adjectif": ["black"],
        "_groupe_nominal": ["_adjectif"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "blanc": {
        "_adjectif": ["white"],
        "_groupe_nominal": ["_adjectif"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "connait": {
        "_verbe": ["knows"],
        "_groupe_verbal": ["_verbe", "_adverbe"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    },
    "bien": {
        "_adverbe": ["well"],
        "_groupe_verbal": ["_verbe", "_adverbe"],
        "_phrase": ["_groupe_nominal", "_groupe_verbal", "_groupe_nominal"]
    }
}


class TALN:
    def __init__(self, sentence, table_analyseLL, table_analyseLL_cible, logger_enabled = True):
        self.table = table_analyseLL
        self.table_cible = table_analyseLL_cible
        self.analyseTable = ["_phrase"]
        self.logger_enabled = logger_enabled
        self.sentence = sentence
        self.curr_group = ''
        self.curr_group_words = []


    def analyse(self):
        self.log(self.sentence, self.analyseTable)
        self.readSentence(self.sentence[0])
        self.log(self.sentence, self.analyseTable)
        tmp_sentence = self.sentence
        for word in tmp_sentence:
            if len(self.analyseTable) == 0 and len(self.sentence) != 0:
                raise Exception('Sommet manquant !')
            elif len(self.analyseTable) != 0 and len(self.sentence) == 0:
                raise Exception('Texte manquant !')
            else:
                self.readWord(word)

    def readWord(self, word):
        if (self.analyseTable[0] == self.sentence[0]):
            self.curr_group_words.append(word)
            self.log(self.sentence, self.analyseTable)
            self.analyseTable = self.analyseTable[1:]
            self.sentence = self.sentence[1:]

            if (len(self.sentence) == 0):
                self.groupTranslate()

            return
        else:
            if (self.analyseTable[0] in ['_groupe_nominal', '_groupe_verbal']):
                #print(self.analyseTable[0])
                self.groupTranslate()
                self.curr_group = self.analyseTable[0]
                self.curr_group_words = []

            self.readRule(word)

        self.readWord(word)
    
    def groupTranslate(self):
        if (self.curr_group):
            first_word_structure = self.table_cible[self.curr_group_words[0]
                                                    ][self.curr_group]

            final_sentence = []

            for word in self.curr_group_words:
                for structure in first_word_structure:
                    final_word = self.cibleFind(word, structure)
                    if (final_word != None):
                        final_sentence.append(final_word)

            print('final : ', final_sentence)

    def cibleFind(self, word, structure):
        if (self.table_cible[word].get(structure)):
            tmp = self.table_cible[word][structure]
            if (tmp[0].find('_') != -1):
                return self.cibleFind(word, tmp[0])
            else:
                return tmp[0]

    def readRule(self, word):
        self.log(self.sentence, self.analyseTable)
        if (self.table[word].get(self.analyseTable[0])):
            self.log('bonne pioche')
            tmp = self.analyseTable
            self.analyseTable = self.table[word][self.analyseTable[0]].copy()
            self.analyseTable.extend(tmp[1:])
        elif (self.analyseTable[0] == '_adverbe' or self.analyseTable[0] == '_adjectif'):
            self.log('<Epsilon>')
            self.analyseTable = self.analyseTable[1:]
        else:
            raise Exception('Manque des trucs (mot : "' + word + '") pour la règle ' + self.analyseTable[0] + " lo !")


    def readSentence(self, word):
        self.analyseTable = self.table[word][self.analyseTable[0]].copy()


    def log(self, *values):
        if (self.logger_enabled):
            print("[DEBUG]", values, "\n")


test = TALN(["Pierre", "connait", "le", "chat", "noir"], table_analyseLL, en_table_analyseLL, False)
test.analyse()
